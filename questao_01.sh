#!/bin/bash

if [ -e $1 ]; then
	echo -e "\nexiste"
else
	echo -e "\nnão existe\n"
	exit 1
fi

echo -e "\n*****MENU*****"
echo -e "\n Opção 1: 10 primeiros\n Opção 2: 10 Ultimos\n Opção 3: Número de linhas\n"
read -p "Digite um número referente ao MENU:  " n1

if [ $n1 = "1" ]; then
	head $1

elif [ $n1 = "2" ]; then
	tail $1

elif [ $n1 = "3" ]; then
	wc -l $1

fi


