#!/bin/bash

total=0
for i in $*; do
	if ping -c 1 $i &> /dev/null; then
		total=$((total+1))
	fi
done

echo $total
